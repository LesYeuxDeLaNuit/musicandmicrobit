
(
{
	// TR-808 Like MicroBit sampler with visual callback.

	var debounceT = 0.3;

	var player, visual, run_script, port_cheker, last_value=0;
	var pathR = "".resolveRelative;
	var sounds = Array.new;
	var soundFile = PathName(pathR+/+"soundFiles");

	soundFile.entries.do({ |path|
		sounds = sounds.add(Buffer.readChannel(s, path.fullPath, channels: [0]));
	});

	port_cheker = {
		var port = NetAddr.langPort;
		if (port != 57120, { thisProcess.openUDPPort(57120) });
	}.value;

	visual = Task({
		var wHight = 150, wWidth = 150;
		var color1 = rrand(0, 255), color2 = rrand(0, 255), color3 = rrand(0, 255);
		var window = Window(
			bounds: Rect(exprand(100, 1600), exprand(100, 1000), wWidth, wHight),
			resizable: false,
			border: false
		).alpha_(0.8);

		window.background_(Color.new255(color1, color2, color3));
		window.front; debounceT.wait;
		window.close;
	});

	SynthDef(\oneShotPlayer, { |out=0, buff, loop=0, rel=1, gate=0, pan=0, amp=1|
		var sig, env;
		env = EnvGen.kr(Env.asr(0.01, 1, rel), gate, doneAction:2);
		sig = PlayBuf.ar(buff.numChannels, buff, BufRateScale.kr(buff), loop: loop, doneAction:2);
		sig = sig * env;

		if (buff.numChannels == 1, {
			sig = Pan2.ar(sig, pos: pan, level: amp.lag(0.05));
		}, {
			sig = Splay.ar(sig, level: amp.lag(0.05), center: pan);
		});

		Out.ar(out, sig);
	}).add;

	s.sync;

	OSCdef(\OnePlay, { |msg|
		var buff = msg[1] -1;

		if (msg[1] >= 1 && player.isPlaying.not, {
				visual.start(AppClock);
				player = Synth(\oneShotPlayer, [\buff, sounds[buff], \gate, 1]).register;
		});

	}, "/play"
	);

}.fork
)


