# MusicAndMicroBit
The project is to be able to intervene on sound with motion.
We have three parts, first send a msg, second translate the signal to computer and change the sound values

1. Send a msg
For sending our msg well use two component in a Micro:bit card, the first is an RF Transmiter and the second is an accelerometer.
* First step:
Detect position of x axis
* Second step:
Send name and position of x axis to my Micro:bit translator

2. Translate the signal

3. change the sound values

## Documentation
* [Micro:bit](https://www.microbit.org/)
* [SuperCollider](https://supercollider.github.io/)